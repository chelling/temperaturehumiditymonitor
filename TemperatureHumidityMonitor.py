# -*- coding: utf-8 -*-
#
# StorageHumidityMonitor: reads and logs SHT21 temperature and humidity sensors
# connected to "16xI2C to Raspberry Pi" board.
#
# Configuration file can be specified with option -c filename.conf
#
# Carlos García Argos, October 2018
#
# https://gitlab.cern.ch/cgarciaa/storage-humidity-monitor
#
# Edit:
# Use pigpio instead of smbus to read HTU21 sensors
# cf. https://www.raspberrypi.org/forums/viewtopic.php?t=84966
#
# Z. Tao, May 2021
#
# EDIT:
# Changed a lot
#
# chelling, December 2021

import pigpio
import helpers
import json
import argparse

parser = argparse.ArgumentParser(
         description = "Read temperature and relative humidity from several SHT21 sensors.")

parser.add_argument('-c',
                    dest    = 'configfile',
                    type    = str,
                    default = '',
                    help    = 'Path to configuration json.'
                    )

parser.add_argument('-o',
                    dest    = 'logfile',
                    type    = str,
                    default = '',
                    help    = 'Path to output log file.'
                    )

args = parser.parse_args()

if __name__ == "__main__":
    
    # path to configuration file
    configfile = args.configfile
    # path to logfile
    logfile    = args.logfile
    
    try:
        conf = json.load(open(configfile))
    except:
        print("Cannot load configfile, using default values.")
        conf = dict()

    # If configuration file specified, set values based on json. 
    # If values not present, use defaults.
    connected_sensors = conf.get('Sensors', dict()).get('connected_sensors', 'autodiscover')
    grafana_active    = conf.get('Grafana', dict()).get('active', False)
    grafana_server    = conf.get('Grafana', dict()).get('server', '206.12.94.115')
    grafana_port      = conf.get('Grafana', dict()).get('port', '8186')
    database          = conf.get('Grafana', dict()).get('database', 'telegraf')

    try:
        pi = pigpio.pi()
        with helpers.TemperatureHumidityMonitor(pi, args) as sht21:
            
            # if sensors not specified, autodiscover them. Can handle any string
            if (connected_sensors == "autodiscover" or type(connected_sensors) == str): 
                connected_sensors = sht21.sensor_autodiscover()
                helpers.edit_configfile(configfile, connected_sensors)
            # scan over listed sensors
            for sensor in connected_sensors:
                
                # currently used to stop spurious results coming from
                # unattached sensors.
                sht21.scan_mux()
                # set proper multiplexer and set channel based on sensor     
                sht21.set_mux(sensor)
                
                # get temperature and humidity readings from sensor 
                sensor_str = 'SHT%s' %sensor
                temperature = sht21.read_temperature()
                humidity    = sht21.read_humidity()
                
                # -999 for either values means an error occured, do not write values
                if ((temperature != -999) and (humidity != -999)):
                    
                    # send data to grafana server
                    if (grafana_active):
                        helpers.send2influxdb(grafana_server, grafana_port, sensor_str, 
                                              temperature, humidity, database)
                    
                # logging
                if (logfile):
                    helpers.logdata(logfile, sensor_str, temperature, humidity)
    except:
        print('Error creating connection to i2c.')
