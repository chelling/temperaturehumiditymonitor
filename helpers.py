import os
import time
import requests
import json
import argparse
import pigpio

def edit_configfile(configfile, connected_sensors):
    '''
    edit_configfile( (string) configfile, (list) connected_sensors)
    
    If the configuration was set to autodiscover, or no sensors
    were listed, this function will edit the config file, adding the 
    connected_sensors field or editing it with the sensors which have
    been found
    '''
    
    with open(configfile, 'r') as f: 
        conf = json.load(f)
  
    conf['Sensors']['connected_sensors'] = connected_sensors

    with open(configfile, 'w') as f:
        json.dump(conf, f, indent = 4)


def logdata(logfile, sensor, temperature, humidity):
    '''
    logdata( (string) logfile, (string) sensor, (float) temperature, (float) humidity)

    Takes sensor name and data and prints to output file for logging
    '''
    # Append data to existing file, create it if it doesn't exist.
    with open(logfile, 'a+') as f:
    
        save_string = str(int(time.time())) + ", " + sensor + ", " + str(temperature) +  ", " + str(humidity)
        
        f.write(save_string)
        f.write('\n')
        f.close()
    

def send2influxdb(server, port, sensor, temperature, humidity, database=""):
    '''
    send2influxdb( (string server, (string) port, (string) sensor_str,
                   (float) temperture, (float) humidity, (string) database)

    This function takes in the sensor values and sends them to out database. The
    function was taken from Carlos Garcia Argos' script and modified by Zhengcheng
    Tao, then modified by Cole Helling

    -- server (string)
        IP address to the virtual machine holding the data

    -- port (string)
        port number the VM is expecting for data

    -- sensor (string)
        name of the sensor

    -- temperature (float)
        value extracted from the I2C readout of the SHT sensor
    
    -- humidity (float)
        value extracted from the I2C readout of the SHT sensor

    -- database (string)
        name of the database
    '''
    
    # destination
    argument = "/write?precision=s"

    if (database):
        argument += "&db=" + database
    
    # via http
    destination = "http://" + server + ":" + port + argument
    
    # data
    hostname = os.uname()[1]
    timestamp = int(time.time())
    
    measurement_name = hostname + "_" + sensor

    data = "{},host={},sensor={} temperature={},humidity={} {}".format(measurement_name, hostname, 
            sensor, temperature, humidity, timestamp)

    # send data to destination
    try:
        requests.post(destination, data = data, timeout = 2.50)
    except:
        pass
    

class TemperatureHumidityMonitor:
    '''
    Reads out the temperature and humidity from one or more 
    SHT21 sensors. 

    __init__
    Variables are assigned via a json file or given a default value
    if not specified. 

    set_mux
    Selects the appropriate multiplexer and sets the communication channel
    for the sensor to be read out

    read_temperature
    Reads bytes from sensor and converts the values to Celsius per the
    sensor's datasheet

    read_humidity
    Reads bytes from sensor and converts the values to percent per the 
    sensor's datasheet
    '''
    
    def __init__(self, pi, args):
        '''
        Initialize variables with values from the configuration file,
        or use defaults if either the configuration file was not specified,
        or the fields are not presesent in the file.
        '''
        self.configfile = args.configfile
        self.logfile    = args.logfile

        try:
            conf = json.load(open(self.configfile))
        except:
            print("Cannot load configfile, using default values.")
            conf = dict()
    
        # Addresses for the multiplexers
        self.sensors_mux1_addr = conf.get('Sensors', dict()).get('mux1_addr', 112)
        self.sensors_mux2_addr = conf.get('Sensors', dict()).get('mux2_addr', 113)

        # Control Constants
        self._SOFTRESET                   = conf.get('Sensors', dict()).get('soft_reset', 254)
        self._I2C_ADDRESS                 = conf.get('Sensors', dict()).get('SHT_address', 64)
        self._TRIGGER_TEMPERATURE_NO_HOLD = conf.get('Sensors', dict()).get('trigger_temp', 243)
        self._TRIGGER_HUMIDITY_NO_HOLD    = conf.get('Sensors', dict()).get('trigger_humi', 245)
        
        self.pi = pi

        # Reset the multiplexers
        self.mux1 = self.pi.i2c_open(1, self.sensors_mux1_addr)
        self.pi.i2c_write_byte(self.mux1, self._SOFTRESET)
        
        self.mux2 = self.pi.i2c_open(1, self.sensors_mux2_addr)
        self.pi.i2c_write_byte(self.mux2, self._SOFTRESET)

        # Reset the SHT sensors
        self.sensor = self.pi.i2c_open(1, self._I2C_ADDRESS)
        self.pi.i2c_write_byte(self.sensor, self._SOFTRESET)

        #scan_mux()

        # Sleep briefly
        time.sleep(0.015)

    def read_temperature(self):
        '''
        Reads in the temperature from the sensor. 
        Note that this call waits 250ms to allow the sensor
        to return the data
        
        The function takes the first two bytes of data and converts
        the temperature in celcius by using the following function:

        T = 46.85 + 172.72 * (sensor_temp / 2^16)

        Per the sensor documentation, the last two bits of the second 
        byte are stat bits, and should be set to zero prior to calculating
        the temperature.
        '''
        
        try:
            # trigger temperature reading
            self.pi.i2c_write_byte(self.sensor, self._TRIGGER_TEMPERATURE_NO_HOLD)
            time.sleep(0.250)

            # read two bytes for data, one for the checksum
            cnt, data = self.pi.i2c_read_device(self.sensor, 3)
            
            if (len(data) == 0):
                raise RuntimeError("Error reading temperature")
            
            # The last two bits (stat bits) of the second byte must be set to zero
            # before calculation. For temperature readings, they are both always
            # 0, so no need to change.
            temp = -46.85 + 175.72*((data[0]*2.0**8) + data[1]) / 2.0**16
        except:
            temp = -999

        return temp


    def read_humidity(self):
        '''
        Reads the humidity from the sensor.
        Note that this call waits 250ms to allow the sensor
        to return the data
        
        The humidity function takes the first two bytes of data and 
        converts the humidity in percent by using the following function:

        RH = -6.0 + 125.0*(sensor_hum / 2^16)
        '''
        try:
            # trigger humidity reading
            self.pi.i2c_write_byte(self.sensor, self._TRIGGER_HUMIDITY_NO_HOLD)
            time.sleep(0.250)

            # read two bytes for data, one for the checksum
            cnt, data = self.pi.i2c_read_device(self.sensor, 3)
            
            if (len(data) == 0):
                raise RuntimeError("Error reading humidity")
            
            # The last two bits (stat bits) of the second byte must be set to zero
            # before calculation. For humidity readings, they are always
            # 10, so we need to subract 2 (i.e. 00000010 = 2).
            humidity = -6.0 + 125.0*((data[0]*2.0**8) + (data[1] - 2))/2.0**16
        
        except:
            humidity = -999

        return humidity

    def scan_mux(self):
        '''
        It seems that if you read from a multiplexer when
        no sensors are present, you get values when you shouldn't.
        By scanning over each channel on both multiplexers, 
        this problem doesn't happen.
        '''
        for i in range(0,8):
            self.pi.i2c_write_byte(self.mux1, 2**i)
            self.pi.i2c_write_byte(self.mux2, 2**i)

    def set_mux(self, idx):
        '''
        set_mux(self, (int) sensor_idx

        This function takes in the sensor index 0, 1, ..., 15. Each
        multiplexer has 8 channels, with addresses in powers of two,
        i.e. 2^0, 2^1, ..., 2^15. For sensors on multiplexer 2 (mux2),
        the index is shifted to give the correct channel address.
        '''
        if (idx < 8):
            self.pi.i2c_write_byte(self.mux1, 2**idx)
        else:
            self.pi.i2c_write_byte(self.mux2, 2**(idx - 8))
    
    def sensor_autodiscover(self):
        '''
        If which sensors connected is not known, this
        function will scan over all sensors and return
        the sensor numbers that have valid values. 
        '''
        # to store connected sensors
        connected_sensors = []
        # scan over sensors
        self.scan_mux()

        # scan channels and look for valid values
        for i in range(0,15):
            self.set_mux(i)
            # get temperature to test
            t = self.read_temperature()
            
            if (t != -999):
                connected_sensors.append(i)
        
        if (len(connected_sensors) == 0):
            print("No sensors were discovered, exiting ...")
            sys.exit()
        else:
            return connected_sensors

    def close(self):
        '''
        Closes the I2C connection
        '''
        self.pi.i2c_close(self.mux1)
        self.pi.i2c_close(self.mux2)
        self.pi.i2c_close(self.sensor)
        self.pi.stop()

    def __enter__(self):
        '''
        Used to enable python with statement support
        '''
        return self

    def __exit__(self, type, value, traceback):
        '''
        With support
        '''
        self.close()
