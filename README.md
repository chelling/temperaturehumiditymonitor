# TemperatureHumidityMonitor

This project allows the monitoring of up to 16 SHT21 temperature humidity
sensors connected to a Rasbperry Pi with a custom made PiHat board.

This project was originally made by [Carlos Garcia
Argos](https://gitlab.cern.ch/cgarciaa/storage-humidity-monitor/-/blob/master/software/storagemonitor.conf)
and was later modified by [Zhengcheng
Tao](https://gitlab.cern.ch/ztao/storage-humidity-monitor/-/tree/master),
switching to pigpio to communicate rather than smbus. I later modified it to
make it a little easier to follow (removing hexadecimal) and made several
heavier modifications.

## Install Dependencies

The I2C protocol is used to communicate with the SHT21 sensors. To communicate
with the I2C bus, the [pigpio](https://pypi.org/project/pigpio/#description)
(Pi General Purpose Input/Output) python library is used.

``` bash
sudo pip3 install pigpio
```

To make the pigpio daemon (pigpiod) start automatically at system startup,
execute the following:

``` bash
sudo systemctl enable pigpiod
```

Other libraries include:
  * os
  * time
  * requests
  * json
  * argparse

## SHT PiHat Board

The PiHat board was designed by Carlos Garcia Argos (whom I thank greatly). It
is designed to connect to up to 16 SHT sensors via 4 16-pin ribbon cables. The
code is designed to work with the
[SHT21](http://www.farnell.com/datasheets/1780639.pdf) temperature and humidity
sensors, and may require a slight reworking if applied to other, similar
sensors.

![](Images/SHT21_PiHat_Board.png)

Starting from the bottom connection in the picture above are the locations for
SHT0 - SHT3. The sensor numbering starts at zero with the four right-most pins:
SDA, SLC, Vin, and GND. The next set of four pins to the left is for SHT1 and
the pattern continues in this manner across the 16-pin connector and as you go
up to SHT15.


## Configuration

A json is used to store the configuration settings to communicate with the
sensors and to send data to the database.

``` json
{
  "Sensors":
  {
    "connected_sensors" : [0, 1],
    "SHT_address"       : 64,
    "mux1_addr"         : 112,
    "mux2_addr"         : 113,
    "soft_reset"        : 254,
    "trigger_temp"      : 243,
    "trigger_humi"      : 245
  },
  "Grafana":
  {
    "active"   : true,
    "server"   : "206.12.94.115",
    "port"     : "8186",
    "database" : "telegraf"
  }
}
```

Originally, the configuration was designed around using hexidecimal rather than
decimal numbers for the addresses, but I have converted it all to decimal to
make it all easier to read for myself. 

The default address for the SHT21 sensors (SHT_address) is 0x40, or 64. The two
8-channel multiplexers (mux1_addr and mux2_addr) have been given the addresses
0x70 and 0x71, or 112 and 113 respectively. The soft-reset command, 0xFE or
254, is used to reset the multiplexers and the sensor. To trigger a temperature
or humidity reading, 0xF3 (243) and 0xF5 (245) are used, respectively. 

The user can list the connected sensors, [0,...,15]. If nothing is given, or
``autodiscover`` (Any string will actually trigger it) is given in the
`connected_sensors` field instead, the program will automatically scan all
channels and edit the configuration for future scans.

We send data to a virtual machine using [InfluxDB](https://www.influxdata.com/)
via the [Telegraf](https://www.influxdata.com/time-series-platform/telegraf/)
server agent. To send data, set the `active` field to `true` and configure the
`server` and `port` fields appropriately. 

## Running the Program

This program was designed to be called as a cronjob every minute. This is done
by opening up the
[crontab](https://www.adminschoice.com/crontab-quick-reference) (in this case
with vim as the editor) using the command:

``` bash
EDITOR=vim crontab -e
```

The following line added will execute the script every minute:

```bash
* * * * * sudo python3 /[path_to_repository]/temperature_humidity_monitor/TemperatureHumidityMonitor.py -c /[path_to_repository]/temperature_humidity_monitor/monitor_configuration.json
```

## Troubleshooting and Logging

### I2C Detection

If your Raspberry Pi is not reading in I2C devices, make sure that I2C
communication has been turned on. Instructions on how to do this are found
[here](https://www.raspberrypi-spy.co.uk/2014/11/enabling-the-i2c-interface-on-the-raspberry-pi/).

To check which devices are connected, use the following command:

``` bash
sudo i2cdetect -y 1
```
If the multiplexers are found, but not the SHT sensor, you'll see the
following output

``` bash
     0  1  2  3  4  5  6  7  8  9  a  b  c  d  e  f
00:          -- -- -- -- -- -- -- -- -- -- -- -- --
10: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
20: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
30: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
40: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
50: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
60: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
70: 70 71 -- -- -- -- -- -- 
```

This can be remedied by sending a soft reset command to the multiplexers. This
is simplest to do using smbus in python:

``` bash
sudo python3
```

``` python
import smbus
bus = smbus.SMBus(1)
bus.write_byte(112, 254)
bus.write_byte(113, 254)
```
Which should produce the following output using the detect command:

``` bash
     0  1  2  3  4  5  6  7  8  9  a  b  c  d  e  f
00:          -- -- -- -- -- -- -- -- -- -- -- -- --
10: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
20: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
30: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
40: 40 -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
50: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
60: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
70: 70 71 -- -- -- -- -- -- 
```

You'll only see one new entry, 40 (0x40 -> 64), the default address of the
SHT21 sensor. If you still cannot detect the sensor, check the wiring, or power
down the Pi and unplug it before rebooting (I've found that helps when I2C is
being obstinate). On one occasion, I found that dozens of addresses were being
detected, this was caused by having the wiring on the sensor backwards.
Correcting this and resetting the multiplexers should resolve this.

### Temperature Readings from Unconnected Sensors

While writing this code, I had a persistent issue. If you try to read sensor
values from a multiplexer not connected to anything, you will sometimes get
valid temperature and humidity readings. My guess is that it is somehow reading
values from the connected sensors despite not being connected to the same
multiplexer, since they tend to be close in value to valid readings. The
problem seems to stop as soon as it reaches a connected sensor. The cause of
this is still a mystery to me, but I found a way to resolve it. I made a
function called `scan_mux` in the TemperatureHumidityMonitor Class that sets
every channel for each of the two multiplexers:

``` python
def scan_mux(self):
    for i in range(0,8):
      self.pi.i2c_write_byte(self.mux1, 2**i)
      self.pi.i2c_write_byte(self.mux2, 2**i)
```

I do not know precisely why, but this resolves the problem and gives me the
expected value for temperature and humidity (default is -999) when no sensor is
connected. If someone finds the answer to this, please message me!

### Logging Readout to a File

Despite what is undoubtedly impeccable coding, there may be issues reading
and sending the data. To output the readings to a log file, add `-o [log_output_filename].txt`
to the python command:

```bash
sudo python3 TemperatureHumidityMonitor.py -c monitor_configuration.json -o output.txt
```

It simply outputs the sensor name, SHTX (X = sensor number), the time stamp,
and the temperature and humidity values. It will print the values only for the
`connected_sensors`, so you should put the any of the sensors to be inspected
into the list explicitly. Using autodiscover when the sensors are failing to be
detected will obviously not produce an output in the log file.
